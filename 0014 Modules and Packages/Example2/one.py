def fun1():
    print("fun 1 in one.py")
    
def fun2():
    print("fun 2 in one.py")
    
def main():
    print("ONE.PY is being run directly")

print("top level in one.py")

if __name__ == "__main__":
    main()
else:
    print("ONE.PY has been imported.")